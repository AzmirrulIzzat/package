#!/bin/sh
. /data/backup/maintenance/properties.ini

if [ $# -eq 0 ]; then
    echo "No new password provided"
    exit 1
fi

NEWPASSWORD=`echo $1 | openssl enc -aes-128-cbc -a -salt -pass pass:$PRIVATEKEY`
#echo "New Encrypted Password = $NEWPASSWORD"

ESCAPED_NEWPASSWORD=$(printf '%s\n' "$NEWPASSWORD" | sed -e 's/[\/&]/\\&/g')
#echo "New Escape Encrypted Password = $ESCAPED_NEWPASSWORD"

sed -i "/^PASSWORD=/s/=.*/=\"${ESCAPED_NEWPASSWORD}\"/" /data/backup/maintenance/properties.ini
echo "Encrypted password has been UPDATED"
