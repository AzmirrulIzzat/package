#!/bin/bash
# Author: Izzat
# Script: start-tar.sh
maintanancefolder="/data/maintenance"
aeminstallfolder="/data/aem"
aemfolder="$aeminstallfolder/crx-quickstart"
logdirectory="$maintanancefolder/compaction-logs" #logs directory
aemport='4502'

today="$(date +'%Y-%m-%d')"
logfile="tarcompact-$today.log"
oakrun="$maintanancefolder/oak-run-1.22.4.jar"

if lsof -Pi :$aemport -sTCP:LISTEN -t > /dev/null; then
        echo "Instance is running."
else
        echo "Ready for compaction."

        ## Shutdown AEM

        printf "Shutting down AEM.\n"

        $aemfolder/bin/stop

        now="$(date)"

        echo "AEM Shutdown at: $now" >> $logdirectory/$logfile

        ## Find old checkpoints

        printf "Finding old checkpoints.\n"

        java -Dtar.memoryMapped=true -Xmx1g -jar $oakrun checkpoints $aemfolder/repository/segmentstore >> $logdirectory/$logfile

        ## Delete unreferenced checkpoints

        printf "Deleting unreferenced checkpoints.\n"

        java -Dtar.memoryMapped=true -Xmx1g -jar $oakrun checkpoints $aemfolder/repository/segmentstore rm-unreferenced >> $logdirectory/$logfile

        ## Run compaction

        printf "Running compaction. This may take a while.\n"
        ## https://docs.adobe.com/docs/en/aem/6-1/deploy/platform/storage-elements-in-aem-6.html
        java -Dtar.memoryMapped=true -Dsun.arch.data.model=32 -Xmx1g -jar $oakrun compact $aemfolder/repository/segmentstore >> $logdirectory/$logfile

        ## Report Completed

        printf "Compaction complete. Please check the log at:\n"

        printf "$logdirectory:q:q/$logfile\n"

        ## Start AEM back up

        now="$(date)"

        printf "Starting up AEM.\n"

        $aemfolder/bin/start

        echo "AEM Startup at: $now" >> $logdirectory/$logfile

        echo "Compaction is running."
fi
