#!/bin/bash
# Author: Izzat
# Description : backup the aem using jmx api and retain backup file until max_file number of file
. /data/backup/maintenance/properties.ini


AEMSTATUSURL="$AEMDOMAIN:$AEMPORT/system/console/jmx/com.adobe.granite%3Atype%3DRepository"

TODAY="$(date +'%Y-%m-%d')"
BACKUPNAME="$FILENAME_PREFIX-$TODAY.zip"

LOGFILE="$LOGNAME_PREFIX-$TODAY.log"
COMLOGSDIR="$BACKUPDIRECTORY/logs" #logs directory

EXISTING_FILE=$(ls -tp $BACKUPDIRECTORY | grep $FILENAME_PREFIX | wc -l)

echo "Max Number of Rotation : $MAX_FILE " >> $COMLOGSDIR/$LOGFILE
echo "Max Existing File : $EXISTING_FILE " >> $COMLOGSDIR/$LOGFILE

if (( EXISTING_FILE > MAX_FILE ))

        then

                echo "before delete activity : $(ls -tp $BACKUPDIRECTORY | grep $FILENAME_PREFIX | wc -l )" >> $COMLOGSDIR/$LOGFILE
                echo "delete old file" >> $COMLOGSDIR/$LOGFILE
                ## remove backup file
                ls -1tr $BACKUPDIRECTORY/$FILENAME_PREFIX* | head -n -$MAX_FILE | xargs -d '\n' rm -f --
                ## remove logs file
                ls -1tr $COMLOGSDIR/$LOGNAME_PREFIX* | head -n -$MAX_FILE | xargs -d '\n' rm -f --
                echo "after delete activity : $(ls -tp $BACKUPDIRECTORY | grep $FILENAME_PREFIX | wc -l )" >> $COMLOGSDIR/$LOGFILE

fi

NOW="$(date)"
echo "AEM Backup delay set to 0 START at: $NOW" >> $COMLOGSDIR/$LOGFILE

curl -u $AEMUSER:`echo $PASSWORD | openssl enc -aes-128-cbc -a -d -salt -pass pass:$PRIVATEKEY` -X POST http://localhost:$AEMPORT/system/console/jmx/com.adobe.granite:type=Repository/a/BackupDelay?value=0 | tee -a $COMLOGSDIR/$LOGFILE

NOW="$(date)"
echo "AEM Backup delay set to 0 END at: $NOW" >> $COMLOGSDIR/$LOGFILE

NOW="$(date)"
echo "AEM Backup START at: $NOW" >> $COMLOGSDIR/$LOGFILE

curl -u $AEMUSER:`echo $PASSWORD | openssl enc -aes-128-cbc -a -d -salt -pass pass:$PRIVATEKEY` -X POST http://localhost:$AEMPORT/system/console/jmx/com.adobe.granite:type=Repository/op/startBackup/java.lang.String?target=$BACKUPDIRECTORY/$BACKUPNAME | tee -a $COMLOGSDIR/$LOGFILE

##curl -u $AEMUSER:$AEMPASSWORD -X POST --data "_charset_=utf-8&delay=1&force=true&installDir=$AEMHOMEDIRECTORY/crx-quickstart/&target=$BACKUPDIRECTORY/$BACKUPNAME" http://localhost:$AEMPORT/libs/granite/backup/content/admin/backups/ | tee -a $COMLOGSDIR/$LOGFILE

echo "AEM Backup STARTED at: $NOW" >> $COMLOGSDIR/$LOGFILE
echo "BACKUP MONITORING PROGRESS AVAILABLE AT $AEMSTATUSURL"
