#!/bin/bash
# Author: Izzat
# Script: start-tar.sh
TARDIRECTORY="/opt/aem65/maintanance/tarcompaction"
TARSCRIPT="$TARDIRECTORY/tarcompact-process.sh"
COMLOGSDIR="$TARDIRECTORY/logs" #logs directory
AEMPORT='5502'

if lsof -Pi :$AEMPORT -sTCP:LISTEN -t > /dev/null; then
	echo "Instance is running."
else
	echo "Ready for compaction."
	nohup $TARSCRIPT > $COMLOGSDIR/tarcompact-start-$(date +%Y-%m-%d).log 2>&1&
	echo "Compaction is running."
fi
