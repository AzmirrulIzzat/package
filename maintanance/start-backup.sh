#!/bin/bash
# Author: Izzat

AEMPORT='5502'
AEMUSER="admin"
AEMPASSWORD="admin"
AEMHOMEDIRECTORY="/opt/aem65/author"
AEMAUTHORDOMAIN="http://serenity.southeastasia.cloudapp.azure.com"
AEMSTATUSURL="$AEMAUTHORDOMAIN:$AEMPORT/libs/granite/backup/content/admin.html"

TODAY="$(date +'%Y-%m-%d')"
BACKUPNAME="aem-backup-$TODAY.zip"
BACKUPDIRECTORY="/opt/aem65/maintanance/backup"

LOGFILE="backup-$TODAY.log"
COMLOGSDIR="$BACKUPDIRECTORY/logs" #logs directory

NOW="$(date)"
echo "AEM Backup delay set to 0 START at: $NOW" >> $COMLOGSDIR/$LOGFILE

curl -u $AEMUSER:$AEMPASSWORD -X POST http://localhost:$AEMPORT/system/console/jmx/com.adobe.granite:type=Repository/a/BackupDelay?value=0 | tee -a $COMLOGSDIR/$LOGFILE

NOW="$(date)"
echo "AEM Backup delay set to 0 END at: $NOW" >> $COMLOGSDIR/$LOGFILE

NOW="$(date)"
echo "AEM Backup START at: $NOW" >> $COMLOGSDIR/$LOGFILE    

curl -u $AEMUSER:$AEMPASSWORD -X POST http://localhost:$AEMPORT/system/console/jmx/com.adobe.granite:type=Repository/op/startBackup/java.lang.String?target=$BACKUPDIRECTORY/$BACKUPNAME | tee -a $COMLOGSDIR/$LOGFILE

##curl -u $AEMUSER:$AEMPASSWORD -X POST --data "_charset_=utf-8&delay=1&force=true&installDir=$AEMHOMEDIRECTORY/crx-quickstart/&target=$BACKUPDIRECTORY/$BACKUPNAME" http://localhost:$AEMPORT/libs/granite/backup/content/admin/backups/ | tee -a $COMLOGSDIR/$LOGFILE

echo "AEM Backup STARTED at: $NOW" >> $COMLOGSDIR/$LOGFILE
echo "BACKUP MONITORING PROGRESS AVAILABLE AT $AEMSTATUSURL"

