#!/bin/bash
## AEM 6.5 Tar compaction for AUTHOR environment.

today="$(date +'%Y-%m-%d')"
logfile="tarcompact-process-$today.log"
aeminstallfolder="/opt/aem65/author"
maintanancefolder="/opt/aem65/maintanance/tarcompaction"
COMLOGSDIR2="$maintanancefolder/logs" #logs directory
aemfolder="$aeminstallfolder/crx-quickstart"
oakrun="$maintanancefolder/tarcompaction/oak-run-1.10.2.jar"

## Shutdown AEM

printf "Shutting down AEM.\n"

$aemfolder/bin/stop

now="$(date)"

echo "AEM Shutdown at: $now" >> $COMLOGSDIR2/$logfile

## Find old checkpoints

printf "Finding old checkpoints.\n"

java -Dtar.memoryMapped=true -Xmx5g -jar $oakrun checkpoints $aemfolder/repository/segmentstore >> $COMLOGSDIR2/$logfile

## Delete unreferenced checkpoints

printf "Deleting unreferenced checkpoints.\n"

java -Dtar.memoryMapped=true -Xmx5g -jar $oakrun checkpoints $aemfolder/repository/segmentstore rm-unreferenced >> $COMLOGSDIR2/$logfile

## Run compaction

printf "Running compaction. This may take a while.\n"
## https://docs.adobe.com/docs/en/aem/6-1/deploy/platform/storage-elements-in-aem-6.html
java -Dtar.memoryMapped=true -Xmx5g -jar $oakrun compact $aemfolder/repository/segmentstore >> $COMLOGSDIR2/$logfile

## Report Completed

printf "Compaction complete. Please check the log at:\n"

printf "$COMLOGSDIR2/$logfile\n"

## Start AEM back up

now="$(date)"

printf "Starting up AEM.\n"

$aemfolder/bin/start

echo "AEM Startup at: $now" >> $COMLOGSDIR2/$logfile
